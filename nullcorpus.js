Log = new Mongo.Collection("log");

Router.route('/', function () {
  this.render('showLog', {
    log : function() {
      return Log.find({});
    }
  });
});

if (Meteor.isClient) {
  Template.showLog.helpers({    
    log : function() {
      return Log.find({});
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
